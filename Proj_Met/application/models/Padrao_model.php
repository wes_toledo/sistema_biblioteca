<?php 

class Padrao_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('padrao_model');
	}

	public function logar(){
		$this->db->select('*')->from('user')->where('login_user',$this->input->post('login'))->where('senha_user',$this->input->post('senha'));
		$res = $this->db->get()->result();
		if($res!=NULL){
			$this->session->set_userdata('id_user',$res[0]->idUsuario);
			$this->session->set_userdata('login_user',$res[0]->login_user);
			$this->session->set_userdata('senha_user',$res[0]->senha_user);
			$this->session->set_userdata('nome_user',$res[0]->nome_user);
			$this->session->set_userdata('logado','1');
			redirect('padrao/inicial');
		}
		redirect('padrao');
		
	}

	public function retorna_dados_grafico(){
		$ano = date('Y');
		$mes = date('m');
		$this->db->select('*')->from('historico_mes')->where('numero_mes',$mes)->where('ano_mes',$ano);
		$res0=$this->db->get()->result();
		if($res0==NULL){
			$cad['qtd0']=0;
		}else{
			$cad['qtd0']=(int)$res0[0]->emprestimos_mensais_mes;
		}
		if($mes=='01'){
			$cad['nom0']="Janeiro";
			$cad['nom1']="Dezembro";
			$cad['nom2']="Novembro";
			$cad['nom3']="Outubro";
			$cad['nom4']="Setembro";
		}else if($mes=='02'){
			$cad['nom0']="Fevereiro";
			$cad['nom1']="Janeiro";
			$cad['nom2']="Dezembro";
			$cad['nom3']="Novembro";
			$cad['nom4']="Outubro";
		}else if($mes=='03'){
			$cad['nom0']="Março";
			$cad['nom1']="Fevereiro";
			$cad['nom2']="Janeiro";
			$cad['nom3']="Dezembro";
			$cad['nom4']="Novembro";
		}else if($mes=='04'){
			$cad['nom0']="Abril";
			$cad['nom1']="Março";
			$cad['nom2']="Fevereiro";
			$cad['nom3']="Janeiro";
			$cad['nom4']="Dezembro";
		}else if($mes=='05'){
			$cad['nom0']="Maio";
			$cad['nom1']="Abril";
			$cad['nom2']="Março";
			$cad['nom3']="Fevereiro";
			$cad['nom4']="Janeiro";
		}else if($mes=='06'){
			$cad['nom0']="Junho";
			$cad['nom1']="Maio";
			$cad['nom2']="Abril";
			$cad['nom3']="Março";
			$cad['nom4']="Fevereiro";
		}else if($mes=='07'){
			$cad['nom0']="Julho";
			$cad['nom1']="Junho";
			$cad['nom2']="Maio";
			$cad['nom3']="Abril";
			$cad['nom4']="Março";
		}else if($mes=='08'){
			$cad['nom0']="Agosto";
			$cad['nom1']="Julho";
			$cad['nom2']="Junho";
			$cad['nom3']="Maio";
			$cad['nom4']="Abril";
		}else if($mes=='09'){
			$cad['nom0']="Setembro";
			$cad['nom1']="Agosto";
			$cad['nom2']="Julho";
			$cad['nom3']="Junho";
			$cad['nom4']="Maio";
		}else if($mes=='10'){
			$cad['nom0']="Outubro";
			$cad['nom1']="Setembro";
			$cad['nom2']="Agosto";
			$cad['nom3']="Julho";
			$cad['nom4']="Junho";
		}else if($mes=='11'){
			$cad['nom0']="Novembro";
			$cad['nom1']="Outubro";
			$cad['nom2']="Setembro";
			$cad['nom3']="Agosto";
			$cad['nom4']="Julho";
		}else if($mes=='12'){
			$cad['nom0']="Dezembro";
			$cad['nom1']="Novembro";
			$cad['nom2']="Outubro";
			$cad['nom3']="Setembro";
			$cad['nom4']="Agosto";
		}
		
		if($mes==01){
			$this->db->select('*')->from('historico_mes')->where('numero_mes','12')->where('ano_mes',$ano-1);
			$res1=$this->db->get()->result();
			if($res1==NULL){
				$cad['qtd1']=0;
			}else{
				$cad['qtd1']=(int)$res1[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes','11')->where('ano_mes',$ano-1);
			$res2=$this->db->get()->result();
			if($res2==NULL){
				$cad['qtd2']=0;
			}else{
				$cad['qtd2']=(int)$res2[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes','10')->where('ano_mes',$ano-1);
			$res3=$this->db->get()->result();
			if($res3==NULL){
				$cad['qtd3']=0;
			}else{
				$cad['qtd3']=(int)$res3[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes','09')->where('ano_mes',$ano-1);
			$res4=$this->db->get()->result();
			if($res4==NULL){
				$cad['qtd4']=0;
			}else{
				$cad['qtd4']=(int)$res4[0]->emprestimos_mensais_mes;
			}
		}else if($mes==02){
			$this->db->select('*')->from('historico_mes')->where('numero_mes','01')->where('ano_mes',$ano);
			$res1=$this->db->get()->result();
			if($res1==NULL){
				$cad['qtd1']=0;
			}else{
				$cad['qtd1']=(int)$res1[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes','12')->where('ano_mes',$ano-1);
			$res2=$this->db->get()->result();
			if($res2==NULL){
				$cad['qtd2']=0;
			}else{
				$cad['qtd2']=(int)$res2[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes','11')->where('ano_mes',$ano-1);
			$res3=$this->db->get()->result();
			if($res3==NULL){
				$cad['qtd3']=0;
			}else{
				$cad['qtd3']=(int)$res3[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes','10')->where('ano_mes',$ano-1);
			$res4=$this->db->get()->result();
			if($res4==NULL){
				$cad['qtd4']=0;
			}else{
				$cad['qtd4']=(int)$res4[0]->emprestimos_mensais_mes;
			}
		}else if($mes==03){
			$this->db->select('*')->from('historico_mes')->where('numero_mes','02')->where('ano_mes',$ano);
			$res1=$this->db->get()->result();
			if($res1==NULL){
				$cad['qtd1']=0;
			}else{
				$cad['qtd1']=(int)$res1[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes','01')->where('ano_mes',$ano);
			$res2=$this->db->get()->result();
			if($res2==NULL){
				$cad['qtd2']=0;
			}else{
				$cad['qtd2']=(int)$res2[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes','12')->where('ano_mes',$ano-1);
			$res3=$this->db->get()->result();
			if($res3==NULL){
				$cad['qtd3']=0;
			}else{
				$cad['qtd3']=(int)$res3[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes','11')->where('ano_mes',$ano-1);
			$res4=$this->db->get()->result();
			if($res4==NULL){
				$cad['qtd4']=0;
			}else{
				$cad['qtd4']=(int)$res4[0]->emprestimos_mensais_mes;
			}
		}else if($mes==04){
			$this->db->select('*')->from('historico_mes')->where('numero_mes','03')->where('ano_mes',$ano);
			$res1=$this->db->get()->result();
			if($res1==NULL){
				$cad['qtd1']=0;
			}else{
				$cad['qtd1']=(int)$res1[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes','02')->where('ano_mes',$ano);
			$res2=$this->db->get()->result();
			if($res2==NULL){
				$cad['qtd2']=0;
			}else{
				$cad['qtd2']=(int)$res2[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes','01')->where('ano_mes',$ano);
			$res3=$this->db->get()->result();
			if($res3==NULL){
				$cad['qtd3']=0;
			}else{
				$cad['qtd3']=(int)$res3[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes','12')->where('ano_mes',$ano-1);
			$res4=$this->db->get()->result();
			if($res4==NULL){
				$cad['qtd4']=0;
			}else{
				$cad['qtd4']=(int)$res4[0]->emprestimos_mensais_mes;
			}
		}else{
			$this->db->select('*')->from('historico_mes')->where('numero_mes',$mes-1)->where('ano_mes',$ano);
			$res1=$this->db->get()->result();
			if($res1==NULL){
				$cad['qtd1']=0;
			}else{
				$cad['qtd1']=(int)$res1[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes',$mes-2)->where('ano_mes',$ano);
			$res2=$this->db->get()->result();
			if($res2==NULL){
				$cad['qtd2']=0;
			}else{
				$cad['qtd2']=(int)$res2[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes',$mes-3)->where('ano_mes',$ano);
			$res3=$this->db->get()->result();
			if($res3==NULL){
				$cad['qtd3']=0;
			}else{
				$cad['qtd3']=(int)$res3[0]->emprestimos_mensais_mes;
			}
			$this->db->select('*')->from('historico_mes')->where('numero_mes',$mes-4)->where('ano_mes',$ano-1);
			$res4=$this->db->get()->result();
			if($res4==NULL){
				$cad['qtd4']=0;
			}else{
				$cad['qtd4']=(int)$res4[0]->emprestimos_mensais_mes;
			}
		}
		
		
		return $cad;
	}

	public function retorna_numero_de_livros(){
		$this->db->select('*')->from('livro');
		return count($this->db->get()->result());
	}

	public function retorna_numero_de_livros_alugados(){
		$this->db->select('*')->from('emprestimo');
		return count($this->db->get()->result());
	}

	public function retorna_numero_de_emprestimos_no_mes(){
		$datamin=date('Y').'-'.date('m').'-01';
		$datamax=date('Y').'-'.date('m').'-'.date('t');
		$this->db->select('*')->from('emprestimo')->where('data_emprestimo >',$datamin)->where('data_emprestimo <',$datamax);
		return count($this->db->get()->result());
	}

	public function retorna_dados_ultimo_mes(){
		if(date('m') == 01){
			$this->db->select('*')->from('historico_mes')->where('numero_mes',01)->where('ano_mes',date('Y'));
		}else{
			$this->db->select('*')->from('historico_mes')->where('numero_mes',date('m')-1)->where('ano_mes',date('Y'));
		}
		return $this->db->get()->result();
	}


	public function retorna_totens(){
		$this->db->select('*')->from('totem');
		return $this->db->get()->result();
	}

	public function retorna_livros_pendentes(){
		$this->db->select('*')->from('livros_pendentes')->join('livro','livro.idLivro = livros_pendentes.idLivro_livros_pendentes');
		return $this->db->get()->result();
	}

	public function deletar_livro(){
		$this->db->select('*')->where('idLivro',$_GET['cod']);
		$this->db->delete('livro');
		redirect("padrao/livros");
	}
	public function acao_altera_emprestimo_emprestado(){
		$this->db->select('*')->where('idEmprestimo',$_GET['cod']);
		$alt = array(
			"emprestado"=>1
		);
		$this->db->update('emprestimo',$alt);
		redirect("padrao/emprestimos");
	}

	public function retorna_devolucoes(){
		$this->db->select('*')->from('devolucao')->join('livro','livro.idLivro = devolucao.idLivro_devolucao')->join('totem','totem.idTotem = devolucao.idTotem_devolucao');
		return $this->db->get()->result();
	}

	public function altera_livros(){
		$cad = array(
			"nome_livro"=>$this->input->post('nomeLivro'),
			"autor_livro"=>$this->input->post('AutorLivro'),
			"id_rfid_livro"=>$this->input->post('rfid')
		);

		$this->db->where('idLivro',$_GET['cod'])->update('livro',$cad);
		
		redirect('padrao/livros');
	}

	public function liberar_pendencia(){
		$this->db->where('idLivrosPendentes',$_GET['cod']);
		$this->db->delete('livros_pendentes');
		redirect('padrao/pendentes');
	}

	public function acao_cadastrar_livro(){
		$cad = array(
			"nome_livro"=>$this->input->post('nomeLivro'),
			"autor_livro"=>$this->input->post('autorLivro'),
			"id_rfid_livro"=>$_GET['rfid']
		);

		$this->db->insert('livro',$cad);
		$this->db->select('*')->where('idLivrosACadastrar',$_GET['cod']);
		$this->db->delete('livros_a_cadastrar');

		$this->db->from('historico_mes')->where('numero_mes',date('m'))->where('ano_mes',date('Y'));
		$a = $this->db->get()->result();
		if($a==NULL){
			$cad = array(
				"numero_mes"=>date('m'),
				"ano_mes"=>date('Y'),
				"qtd_livros_mes"=>1
			);
			$this->db->insert('historico_mes',$cad);
		}else{
			
			$cad = array(
				"qtd_livros_mes"=>$a[0]->qtd_livros_mes + 1
			);
			
			$this->db->where('ano_mes',date('Y'))->where('numero_mes',date('m'))->update('historico_mes',$cad);

		}
		
		redirect('padrao/livros');
	}

	public function altera_totens(){
		$cad = array(
			"nome_totem"=>$this->input->post('nomeTotem'),
			"numero_serie_totem"=>$this->input->post('numeroSerie')
		);

		$this->db->where('idTotem',$_GET['cod'])->update('totem',$cad);
		
		redirect('padrao/totens');
	}

	public function retorna_livro_por_codigo(){
		$this->db->select('*')->from('livro')->where('idLivro',$_GET['cod']);
		return $this->db->get()->result();
	}

	public function retorna_totem_por_codigo(){
		$this->db->select('*')->from('totem')->where('idTotem',$_GET['cod']);
		return $this->db->get()->result();
	}

	public function retorna_livros_prontos_para_cadastro(){
		$this->db->select('*')->from('livros_a_cadastrar');
		return $this->db->get()->result();
	}

	public function retorna_livro_a_ser_cadastrado_por_codigo(){
		$this->db->select('*')->from('livros_a_cadastrar')->where('idLivrosACadastrar',$_GET['cod']);
		return $this->db->get()->result();
	}

	public function retorna_emprestimos(){
		$this->db->select('*')->from('emprestimo')->join('livro','livro.idLivro = emprestimo.idLivro_Emprestimo');
		return $this->db->get()->result();
	}

	public function retorna_emprestimos_false(){
		$this->db->select('*')->from('emprestimo')->where('emprestado',0)->join('livro','livro.idLivro = emprestimo.idLivro_Emprestimo');
		return $this->db->get()->result();
	}

	public function retorna_livros(){
		$this->db->select('*')->from('livro');
		return $this->db->get()->result();
	}

	public function retorna_livros_totem(){
		$this->db->select('*')->from('devolucao')->join('livro','livro.idLivro = devolucao.idLivro_devolucao')->join('totem','totem.idTotem = devolucao.idTotem_devolucao')->where('devolucao.idTotem_devolucao',$_GET['cod']);
		return $this->db->get()->result();
	}
}

?>
