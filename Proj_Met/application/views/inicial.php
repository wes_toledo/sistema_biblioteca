<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<title>Dashboard - Biblioteca</title>
	<link rel="apple-touch-icon" href="<?= base_url() ?>/app-assets/images/favicon/apple-touch-icon-152x152.png">
	<link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>/app-assets/images/favicon/book.png">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined|" rel="stylesheet">
	<!-- BEGIN: VENDOR CSS-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/app-assets/vendors/vendors.min.css">
	<!-- END: VENDOR CSS-->
	<!-- BEGIN: Page Level CSS-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/app-assets/css/themes/vertical-gradient-menu-template/materialize.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/app-assets/css/themes/vertical-gradient-menu-template/style.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/app-assets/css/pages/dashboard.css">
	<!-- END: Page Level CSS-->
	<!-- BEGIN: Custom CSS-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/app-assets/css/custom/custom.css">
	<!-- END: Custom CSS-->
</head>
<!-- END: Head-->

<body class="vertical-layout page-header-light vertical-menu-collapsible vertical-gradient-menu 2-columns  " data-open="click" data-menu="vertical-gradient-menu" data-col="2-columns">

	<!-- BEGIN: Header-->
	<header class="page-topbar" id="header">
		<div class="navbar navbar-fixed">
			<nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-light">
				<div class="nav-wrapper">

					<ul class="navbar-list right">
						<li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a></li>
						<li class="hide-on-large-only"><a class="waves-effect waves-block waves-light search-button" href="javascript:void(0);"><i class="material-icons">search</i></a></li>
						<!-- <li><a class="waves-effect waves-block waves-light notification-button" href="javascript:void(0);" data-target="notifications-dropdown"><i class="material-icons">notifications_none<small class="notification-badge">2</small></i></a></li> -->
						<li>
							<a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown">
								<span class="avatar-status avatar-online">
									<img src="<?= base_url() ?>/app-assets/images/avatar/avatar.png" alt="avatar">
									<i></i>
								</span></a></li>

							</ul>
							<!-- notifications-dropdown-->
							<ul class="dropdown-content" id="notifications-dropdown">
								<li>
									<h6>Notificações<span class="new badge">2</span></h6>
								</li>
								<li class="divider"></li>
								<li><a class="grey-text text-darken-2" href="<?= base_url() ?>/../../#!"><span class="material-icons icon-bg-circle cyan small">add_shopping_cart</span> Um novo livro foi depositado em um totem!</a>
									<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 horas atrás</time>
								</li>
								<li><a class="grey-text text-darken-2" href="<?= base_url() ?>/../../#!"><span class="material-icons icon-bg-circle red small">stars</span> O Totem 2 está lotado!</a>
									<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">5 horas atrás</time>
								</li>

							</ul>
							<!-- profile-dropdown-->
							<ul class="dropdown-content" id="profile-dropdown">
								<!-- <li><a class="grey-text text-darken-1" href="<?= base_url() ?>/../../user-profile-page.html"><i class="material-icons">person_outline</i> Perfil</a></li> -->
								<li><a class="grey-text text-darken-1" href="<?= base_url() ?>/padrao/deslogar"><i class="material-icons">keyboard_tab</i> Deslogar</a></li>
							</ul>
						</div>

					</nav>
				</div>
			</header>
			<!-- END: Header-->


			<!-- BEGIN: SideNav-->
			<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-dark gradient-45deg-deep-purple-blue sidenav-gradient sidenav-active-rounded">
				<div class="brand-sidebar">
					<h1 class="logo-wrapper">
						<a class="brand-logo darken-1" href="<?= base_url() ?>padrao/inicial">
							<img src="<?= base_url() ?>/app-assets/images/logo/open-book.png">
							<span class="logo-text hide-on-med-and-down">Biblioteca</span>
						</a>
						<a class="navbar-toggler" href="#">
							<i class="material-icons">radio_button_checked</i>
						</a>
					</h1>
				</div>

				<ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">


					<li class="navigation-header"><a class="navigation-header-text">Controle</a><i class="navigation-header-icon material-icons">more_horiz</i>
					</li>
			<!--
            <li class="bold"><a class="waves-effect waves-cyan " href="app-email.html"><i class="material-icons">mail_outline</i><span class="menu-title" data-i18n="">Mail</span><span class="badge new badge pill pink accent-2 float-right mr-10">5</span></a>
            </li>-->

            <li class="active bold">
            	<a class="waves-effect waves-cyan " href="<?= base_url() ?>padrao/totens"><i class="material-icons">settings_remote</i><span class="menu-title" data-i18n="">Totens</span></a>
            </li>

            <li class="active bold">
            	<a class="waves-effect waves-cyan" href="<?= base_url() ?>padrao/livros"><i class="material-icons-outlined">book</i><span class="menu-title" data-i18n="">Livros</span></a>
            </li>
            <li class="navigation-header"><a class="navigation-header-text">Acervo</a><i class="navigation-header-icon material-icons">more_horiz</i>
            </li>
            <li class="active bold">
            	<a class="waves-effect waves-cyan" href="<?= base_url() ?>padrao/devolucoes"><i class="material-icons-outlined">book</i><span class="menu-title" data-i18n="">Devoluções</span></a>
            </li>
            <li class="active bold">
            	<a class="waves-effect waves-cyan" href="<?= base_url() ?>padrao/pendentes"><i class="material-icons-outlined">settings_backup_restore</i><span class="menu-title" data-i18n="">Pendentes</span></a>
            </li>
            <li class="active bold">
            	<a class="waves-effect waves-cyan" href="<?= base_url() ?>padrao/emprestimos"><i class="material-icons-outlined">exit_to_app</i><span class="menu-title" data-i18n="">Empréstimos</span></a>
            </li>
        </ul>
        <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
    </aside>
    <!-- END: SideNav-->
    <!-- END: SideNav-->


    <!-- BEGIN: Page Main-->
    <div id="main">
    	<div class="row">
    		<div class="col s12">
    			<div class="container">
    				<!--card stats start-->
					<!-- <div class="card-content">
						<div id="card-stats" class="row">

							 <div class="col s12 m6 xl3">
								<div class="card">
									<div class="card-content red accent-2 white-text">
										<p class="card-stats-title"><i class="material-icons">attach_money</i>17 atrasos</p>
										<h4 class="card-stats-number white-text">R$ 67,00</h4>
										<p class="card-stats-compare">
											<i class="material-icons">keyboard_arrow_up</i> 10%
											<span class="red-text text-lighten-5">mês passado</span>
										</p>
									</div>
									<div class="card-action red">
										<div id="sales-compositebar" class="center-align"></div>
									</div>
								</div>
							</div>
							<div class="col s12 m6 xl4">
								<div class="card">
									<div class="card-content orange lighten-1 white-text">
										<p class="card-stats-title"><i class="material-icons">trending_up</i>N° Livros</p>
										<h4 class="card-stats-number white-text">1054</h4>
										<p class="card-stats-compare">
											<i class="material-icons">keyboard_arrow_up</i> 2%
											<span class="orange-text text-lighten-5">mês passado</span>
										</p>
									</div>
									<div class="card-action orange">
										<div id="profit-tristate" class="center-align"></div>
									</div>
								</div>
							</div>
							<div class="col s12 m6 xl4">
								<div class="card">
									<div class="card-content green lighten-1 white-text">
										<p class="card-stats-title"><i class="material-icons">content_copy</i> Livros alugados</p>
										<h4 class="card-stats-number white-text">304</h4>
										<p class="card-stats-compare">
											<i class="material-icons">keyboard_arrow_down</i> 3%
											<span class="green-text text-lighten-5">mês passado</span>
										</p>
									</div>
									<div class="card-action green">
										<div id="invoice-line" class="center-align"></div>
									</div>
								</div>
							</div>

							<div class="col s12 m6 xl4">
								<div class="card">
									<div class="card-content cyan white-text">
										<p class="card-stats-title"><i class="material-icons">person_outline</i> Usuários recentes</p>
										<h4 class="card-stats-number white-text">186</h4>
										<p class="card-stats-compare">
											<i class="material-icons">keyboard_arrow_up</i> 15%
											<span class="cyan text text-lighten-5">mês passado</span>
										</p>
									</div>
									<div class="card-action cyan darken-1">
										<div id="clients-bar" class="center-align"></div>
									</div>
								</div>
							</div>
						</div>
					</div> -->

					<div id="daily-data-chart">
						<div class="row">
							<div class="col s12 m4 l4">
								<div class="card pt-0 pb-0 animate fadeLeft">
									<div class="padding-2 ml-2">
										<span class="badge gradient-45deg-light-blue-cyan gradient-shadow mt-2 mr-2" style="font-size: .8rem"><?php if($numero_de_livros - $dados_ultimo_mes->qtd_livros_mes >= 0){ ?>
											+
										<?php } ?><?= $numero_de_livros - $dados_ultimo_mes->qtd_livros_mes  ?></span>
										<p class="mt-2 mb-0">Acervo de Livros</p>
										<p class="no-margin grey-text lighten-3">Quantidade de livros cadastrados</p>
										<h5><?=$numero_de_livros?></h5>
										
									</div>
								</div>
								
								
							</div>
							

							<div class="col s12 m4 l4">
								<div class="card pt-0 pb-0 animate fadeRight">
									<div class="padding-2 ml-2">
										<span class="badge gradient-45deg-purple-deep-orange gradient-shadow mt-2 mr-2" style="font-size: .8rem"><?php if($numero_de_emprestimos_mes - $dados_ultimo_mes->emprestimos_mensais_mes >= 0){ ?>
											+
											<?php } ?><?= $numero_de_emprestimos_mes - $dados_ultimo_mes->emprestimos_mensais_mes  ?></span>
											<p class="mt-2 mb-0">Empréstimos</p>
											<p class="no-margin grey-text lighten-3">Empréstimos no mês</p>
											<h5><?=$numero_de_emprestimos_mes?></h5>
										</div>
									<!--
										<div class="row">
											<div class="sample-chart-wrapper" style="margin-bottom: -14px; margin-top: -75px;">
												<canvas id="custom-line-chart-sample-three" class="center"></canvas>
											</div>
										</div>
									-->
								</div>
							</div>

							<div class="col s12 m4 l4">
								<div class="card pt-0 pb-0 animate fadeRight">
									<div class="padding-2 ml-2">
										<span class=" badge gradient-45deg-amber-amber gradient-shadow mt-2 mr-2" style="font-size: .8rem"><?php if($numero_de_livros_alugados - $dados_ultimo_mes->livros_alugados_mes >= 0){ ?>
											+
										<?php } ?><?= $numero_de_livros_alugados - $dados_ultimo_mes->livros_alugados_mes  ?></span>
											<p class="mt-2 mb-0">Alugados</p>
											<p class="no-margin grey-text lighten-3">Livros alugados</p>
											<h5><?=$numero_de_livros_alugados?></h5>
										</div>
									<!--
                                    <div class="row">
                                        <div class="sample-chart-wrapper" style="margin-bottom: -14px; margin-top: -75px;">
                                            <canvas id="custom-line-chart-sample-three" class="center"></canvas>
                                        </div>
                                    </div>-->
								</div>
							</div>
							
					<!--card stats end-->
					<!--yearly & weekly revenue chart start-->
					<div id="sales-chart">
						<div class="">
						

                            <!--card stats end-->
                            <!--yearly & weekly revenue chart start-->
                            <div id="sales-chart">
                            	<div class="row">
                            		<div class="col s12 m12 l12">
                            			<div id="revenue-chart" class="card animate fadeUp">
                            				<div class="card-content">
                            					<h4 class="header mt-0">
                            						Empréstimos
                            						<span class="purple-text small text-darken-1 ml-1">
                            							<?php $porc = (100.0/$dados_grafico['qtd1']) * $dados_grafico['qtd0']; ?>
                            							<i class="material-icons">
                            								<?php if($dados_grafico['qtd1'] > $dados_grafico['qtd0']){ $porc = 100 - $porc; ?>
                            									keyboard_arrow_down
                            								<?php }else{ ?>
                            									keyboard_arrow_up
                            								<?php } ?>
                            							</i> <?= $porc ?> %</span>
                            							
                            						</h4>
                            						<div class="row">
                            							<div class="col s12">
                            								<div class="yearly-revenue-chart">
                            									<canvas id="thisYearRevenue" class="firstShadow" height="350"></canvas>
                            									<canvas id="lastYearRevenue" height="350"></canvas>
                            								</div>
                            							</div>
                            						</div>
                            					</div>
                            				</div>
                            			</div>

                            		</div>
                            	</div>
                            	<!--yearly & weekly revenue chart end-->


                            	<!-- END RIGHT SIDEBAR NAV -->

                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
       			
				   var thisYearData = {
				      labels: ["<?= $dados_grafico['nom4'] ?>", "<?= $dados_grafico['nom3'] ?>", "<?= $dados_grafico['nom2'] ?>", "<?= $dados_grafico['nom1'] ?>", "<?= $dados_grafico['nom0'] ?>"],
				      datasets: [
				         {
				            label: "Quantidade",
				            data: [<?= $dados_grafico['qtd4'] ?>, <?= $dados_grafico['qtd3'] ?>, <?= $dados_grafico['qtd2'] ?>, <?= $dados_grafico['qtd1'] ?>, <?= $dados_grafico['qtd0'] ?>],
				            fill: true,
				            pointRadius: 2.2,
				            pointBorderWidth: 1,
				            borderColor: "#9C2E9D",
				            borderWidth: 5,
				            pointBorderColor: "#9C2E9D",
				            pointHighlightFill: "#9C2E9D",
				            pointHoverBackgroundColor: "#9C2E9D",
				            pointHoverBorderWidth: 2
				         }
				      ]
				   };
                </script>
                <!-- END: Footer-->
                <!-- BEGIN VENDOR JS-->
                <script src="<?= base_url() ?>/app-assets/js/vendors.min.js" type="text/javascript"></script>
                <!-- BEGIN VENDOR JS-->
                <!-- BEGIN PAGE VENDOR JS-->
                <script src="<?= base_url() ?>/app-assets/vendors/chartjs/chart.min.js"></script>
                <!-- END PAGE VENDOR JS-->
                <!-- BEGIN THEME  JS-->
                <script src="<?= base_url() ?>/app-assets/js/plugins.js" type="text/javascript"></script>
                <script src="<?= base_url() ?>/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
                <script src="<?= base_url() ?>/app-assets/js/scripts/customizer.js" type="text/javascript"></script>
                <!-- END THEME  JS-->
                <!-- BEGIN PAGE LEVEL JS-->
                <script src="<?= base_url() ?>/app-assets/js/scripts/dashboard-ecommerce.js" type="text/javascript"></script>
                <!-- END PAGE LEVEL JS-->
            </body>

            </html>
