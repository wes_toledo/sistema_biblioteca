<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Totens - Biblioteca</title>
    <link rel="apple-touch-icon" href="<?=base_url()?>/app-assets/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url()?>/app-assets/images/favicon/book.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined|" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>/app-assets/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>/app-assets/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>/app-assets/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>/app-assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>/app-assets/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>/app-assets/css/themes/vertical-gradient-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>/app-assets/css/themes/vertical-gradient-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>/app-assets/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>/app-assets/css/custom/custom.css">
    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->

<body class="vertical-layout page-header-light vertical-menu-collapsible vertical-gradient-menu 2-columns  " data-open="click" data-menu="vertical-gradient-menu" data-col="2-columns">

    <header class="page-topbar" id="header">
    <div class="navbar navbar-fixed"> 
      <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-light">
        <div class="nav-wrapper">
          
          <ul class="navbar-list right">
            <li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a></li>
            <li>
							<a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown">
								<span class="avatar-status avatar-online">
									<img src="<?=base_url()?>/app-assets/images/avatar/avatar.png" alt="avatar">
									<i></i>
								</span></a></li>
          </ul>
          <!-- translation-button-->
         
          <!-- notifications-dropdown-->
          <ul class="dropdown-content" id="notifications-dropdown">
            <li>
              <h6>Notificações<span class="new badge">2</span></h6>
            </li>
            <li class="divider"></li>
            <li><a class="grey-text text-darken-2" href="<?=base_url()?>/../../#!"><span class="material-icons icon-bg-circle cyan small">add_shopping_cart</span> Um novo livro foi depositado em um totem!</a>
              <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 horas atrás</time>
            </li>
            <li><a class="grey-text text-darken-2" href="<?=base_url()?>/../../#!"><span class="material-icons icon-bg-circle red small">stars</span> O Totem 2 está lotado!</a>
              <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">5 horas atrás</time>
            </li>
            
          </ul>
          <!-- profile-dropdown-->
          <ul class="dropdown-content" id="profile-dropdown">
            <!-- <li><a class="grey-text text-darken-1" href="<?=base_url()?>/../../user-profile-page.html"><i class="material-icons">person_outline</i> Perfil</a></li> -->
            <li><a class="grey-text text-darken-1" href="<?=base_url()?>/padrao/deslogar"><i class="material-icons">keyboard_tab</i> Deslogar</a></li>
          </ul>
        </div>
        
      </nav>
    </div>
  </header>



  <!-- BEGIN: SideNav-->
    <aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-dark gradient-45deg-deep-purple-blue sidenav-gradient sidenav-active-rounded">
        <div class="brand-sidebar">
            <h1 class="logo-wrapper">
                <a class="brand-logo darken-1" href="<?=base_url()?>padrao/inicial">
                    <img src="<?=base_url()?>/app-assets/images/logo/open-book.png">
                    <span class="logo-text hide-on-med-and-down">Biblioteca</span>
                </a>
                <a class="navbar-toggler" href="#">
                    <i class="material-icons">radio_button_checked</i>
                </a>
            </h1>
        </div>

        <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">


            <li class="navigation-header"><a class="navigation-header-text">Controle</a><i class="navigation-header-icon material-icons">more_horiz</i>
            </li>
            <!--
            <li class="bold"><a class="waves-effect waves-cyan " href="app-email.html"><i class="material-icons">mail_outline</i><span class="menu-title" data-i18n="">Mail</span><span class="badge new badge pill pink accent-2 float-right mr-10">5</span></a>
            </li>-->

            <li class="active bold">
                <a class="waves-effect waves-cyan " href="<?=base_url()?>padrao/totens"><i class="material-icons">settings_remote</i><span class="menu-title" data-i18n="">Totens</span></a>
            </li>

            <li class="active bold">
                <a class="waves-effect waves-cyan active" href="<?=base_url()?>padrao/livros"><i class="material-icons-outlined">book</i><span class="menu-title" data-i18n="">Livros</span></a>
            </li>
            <li class="navigation-header"><a class="navigation-header-text">Acervo</a><i class="navigation-header-icon material-icons">more_horiz</i>
            </li>
            <li class="active bold">
                <a class="waves-effect waves-cyan" href="<?=base_url()?>padrao/devolucoes"><i class="material-icons-outlined">book</i><span class="menu-title" data-i18n="">Devoluções</span></a>
            </li>
            <li class="active bold">
                <a class="waves-effect waves-cyan" href="<?=base_url()?>padrao/emprestimos"><i class="material-icons-outlined">exit_to_app</i><span class="menu-title" data-i18n="">Empréstimos</span></a>
            </li>
        </ul>
        <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
    </aside>

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0">Livros</h5>
                        </div>
                        <div class="col s12 m6 l6 right-align-md">
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Livros
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <div class="section section-data-tables">

                        <!--
                        <div class="card">
                            <div class="card-content">
                                <p class="caption mb-0">Tables are a nice way to organize a lot of data. We provide a few utility classes to help
                                    you style your table as easily as possible. In addition, to improve mobile experience, all tables on
                                    mobile-screen widths are centered automatically.</p>
                            </div>
                        </div>-->

                        <!-- DataTables example -->
                        <div class="row">
                                <div class="col s12 m12 l12">
                                <div id="button-trigger" class="card card card-default scrollspy">
                                    <div class="card-content">
                                        <h4 class="card-title">Livros Armazenados - Prédio H</h4>
                                        <div class="row">
                                            <div class="col s12">
                                                <?php foreach($tot as $key => $value){ ?>
                                                <form method="post" action="<?=base_url()?>padrao/acao_altera_totens?cod=<?=$value->idTotem?>">
                                                    ID:<input required value="<?=$value->idTotem?>" type="text" name="codigo" disabled>
                                                    Nome:<input required value="<?=$value->nome_totem?>"  type="text" name="nomeTotem">
                                                    
                                                    Numero de Serie:<input required value="<?=$value->numero_serie_totem?>"  type="text" name="numeroSerie">
                                                    
                                                    Lotacao Maxima:<input required value="<?=$value->maximo_totem?>" type="text" name="maximo" disabled>

                                                    Lotacao Atual:<input required value="<?=$value->lotacao_totem?>" type="text" disabled>
                                                    
                                                    <input class="btn gradient-45deg-deep-purple-blue" value="Salvar" type="submit" name="">
                                                </form>
                                                <?php } ?>
                                                

                                                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        

                        <!-- END RIGHT SIDEBAR NAV -->
                        <!--<div style="bottom: 50px; right: 19px;" class="fixed-action-btn direction-top"><a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow"><i class="material-icons">add</i></a>
                        <ul>
                            <li><a href="css-helpers.html" class="btn-floating blue"><i class="material-icons">help_outline</i></a></li>
                            <li><a href="cards-extended.html" class="btn-floating green"><i class="material-icons">widgets</i></a></li>
                            <li><a href="<?=base_url()?>/../../app-calendar.html" class="btn-floating amber"><i class="material-icons">today</i></a></li>
                            <li><a href="<?=base_url()?>/../../app-email.html" class="btn-floating red"><i class="material-icons">mail_outline</i></a></li>
                        </ul>
                    </div>-->
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Page Main-->

        <!-- Theme Customizer -->

        <!--<a href="https://1.envato.market/materialize_admin" target="_blank" class="btn btn-buy-now gradient-45deg-indigo-purple gradient-shadow white-text tooltipped buy-now-animated tada" data-position="left" data-tooltip="Buy Now!"><i class="material-icons">add_shopping_cart</i></a>
-->
        <!-- BEGIN: Footer-->

        <!--<footer class="page-footer footer footer-static footer-light navbar-border navbar-shadow">
        <div class="footer-copyright">
            <div class="container"><span>&copy; 2019 <a href="https://1.envato.market/pixinvent_portfolio" target="_blank">PIXINVENT</a> All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a href="https://1.envato.market/materialize_admin">PIXINVENT</a></span></div>
        </div>
    </footer>
-->
        <!-- END: Footer-->
        <!-- BEGIN VENDOR JS-->
        <script src="<?=base_url()?>/app-assets/js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="<?=base_url()?>/app-assets/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>/app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>/app-assets/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN THEME  JS-->
        <script src="<?=base_url()?>/app-assets/js/plugins.js" type="text/javascript"></script>
        <script src="<?=base_url()?>/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
        <script src="<?=base_url()?>/app-assets/js/scripts/customizer.js" type="text/javascript"></script>
        <!-- END THEME  JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="<?=base_url()?>/app-assets/js/scripts/data-tables.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
</body>

</html>
