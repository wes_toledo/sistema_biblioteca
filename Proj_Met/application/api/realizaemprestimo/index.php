<?php
	header("Content-type: application/json; charset=utf-8");
	
	include("../conexao.php");
	
	$idLivroEmprestimo = $_POST["idLivroEmprestimo"];

	$response = new stdClass();
	
	$result = $conn->query("SELECT l.idLivro FROM livro AS l  WHERE l.idLivro = $idLivroEmprestimo");
	if ($result->num_rows > 0) {
		$sql = 'INSERT INTO emprestimo(idLivro_Emprestimo, data_emprestimo, horario_emprestimo) VALUES('.$idLivroEmprestimo.',"'.date('d/m/Y').'","'.date('H:i:s').'")';
		if ($conn->query($sql) === TRUE) {
			$a2 = $conn->query('SELECT * FROM historico_mes WHERE numero_mes == '.date('m').' AND ano_mes == '.date('Y'));
			if($a2->num_rows > 0){
				$a3 = $conn->query('SELECT * FROM historico_mes WHERE numero_mes == '.date('m').' AND ano_mes == '.date('Y'));
				$conn->query('UPDATE historico_mes SET emprestimos_mensais_mes = '.$a3->emprestimos_mensais_mes+1);
			}else{
				$conn->query('INSERT INTO historico_mes(emprestimos_mensais_mes) VALUES(1)');
			}
			$response->status = 'OK';
			$response->erro = FALSE;
			echo json_encode($response);
		}else{
			$response->erro = TRUE;
			$response->status = 'Erro_Insert';		
			echo json_encode($response);
		}	
	}else{
		$response->erro = TRUE;
		$response->status = 'Erro_Livro_Nao_Existe';
		echo json_encode($response);
	}
?>
