<?php
	header("Content-type: application/json; charset=utf-8");
	
	include("../conexao.php");
	
	$rfidTag = $_POST["rfidTag"];
	$idTotem = $_POST["idTotem"];

	$response = new stdClass();
	
	$result = $conn->query("SELECT l.idLivro FROM livro AS l  WHERE l.idLivro = '".$rfidTag."'");

	if ($result->num_rows > 0) {
		$sql = 'INSERT INTO devolucao(idTotem_devolucao, idLivro_devolucao, horario_devolucao, data_devolucao) 
		VALUES('.$idTotem.','.$idLivro.',"'.date('H:i:s').'","'.date('d/m/Y').'")';

		if ($conn->query($sql) === TRUE) {
			$a2 = $conn->query('SELECT * FROM historico_mes WHERE numero_mes == '.date('m').' AND ano_mes == '.date('Y'));
			if($a2->num_rows > 0){
				$a3 = $conn->query('SELECT * FROM historico_mes WHERE numero_mes == '.date('m').' AND ano_mes == '.date('Y'));
				$conn->query('UPDATE historico_mes SET livros_alugados_mes = '.$a3->livros_alugados_mes+1);
			}else{
				$conn->query('INSERT INTO historico_mes(livros_alugados_mes) VALUES(1)');
			}
			
			$response->status = 'OK';
			$response->erro = FALSE;
			echo json_encode($response);
		}else{
			$response->erro = TRUE;
			$response->status = 'Erro_Insert';		
			echo json_encode($response);
		}	
	}else{
		$response->erro = TRUE;
		$response->status = 'Erro_Livro_Nao_Existe';
		echo json_encode($response);
	}
?>
