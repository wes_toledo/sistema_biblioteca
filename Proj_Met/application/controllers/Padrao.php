<?php 

class Padrao extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('padrao_model');
	}

	public function index(){
		$this->load->view('header');
		$this->load->view('login');
	}
	public function inicial(){
		$var['dados_grafico']=$this->padrao_model->retorna_dados_grafico();
		$var['numero_de_livros_alugados']=$this->padrao_model->retorna_numero_de_livros_alugados();
		$var['dados_ultimo_mes']=$this->padrao_model->retorna_dados_ultimo_mes()[0];
		$var['numero_de_livros']=$this->padrao_model->retorna_numero_de_livros();
		$var['numero_de_emprestimos_mes']=$this->padrao_model->retorna_numero_de_emprestimos_no_mes();
		$this->load->view('inicial',$var);
	}
	public function devolucoes(){
		$var['dev']=$this->padrao_model->retorna_devolucoes();
		$this->load->view('devolucoes',$var);
	}
	public function emprestimos(){
		$var['emp']=$this->padrao_model->retorna_emprestimos_false();
		$this->load->view('emprestimos',$var);
	}
	public function livros(){
		$var['liv']=$this->padrao_model->retorna_livros();
		$this->load->view('livros',$var);
	}
	public function totens(){
		$var['tot']=$this->padrao_model->retorna_totens();
		$this->load->view('totens',$var);
	}
	public function totens_list(){
		$var['liv']=$this->padrao_model->retorna_livros_totem();
		$var['tot']=$this->padrao_model->retorna_totens();
		$this->load->view('totens',$var);
	}
	public function alterar_livros(){
		$var['liv']=$this->padrao_model->retorna_livro_por_codigo();
		$this->load->view('preencheAlteracaoLivros',$var);
	}
	public function cadastrarLivro(){
		$var['liv']=$this->padrao_model->retorna_livros_prontos_para_cadastro();
		$this->load->view('cadastroLivros',$var);
	}
	public function pendentes(){
		$var['liv']=$this->padrao_model->retorna_livros_pendentes();
		$this->load->view('pendentes',$var);
	}
	public function preencheAlteracaototens(){
		$var['tot']=$this->padrao_model->retorna_totem_por_codigo();
		$this->load->view('preencheAlteracaototens',$var);
	}
	public function preencheDadosLivros(){
		$var['liv']=$this->padrao_model->retorna_livro_a_ser_cadastrado_por_codigo();
		$this->load->view('preencheDadosLivros',$var);
	}
	public function logar(){
		$this->padrao_model->logar();
	}
	public function deletar_livro(){
		$this->padrao_model->deletar_livro();
	}

	public function acao_cadastrar_livro(){
		$this->padrao_model->acao_cadastrar_livro();
	}
	public function liberar_pendencia(){
		$this->padrao_model->liberar_pendencia();
	}
	public function acao_altera_livros(){
		$this->padrao_model->altera_livros();
	}
	public function acao_altera_emprestimo_emprestado(){
		$this->padrao_model->acao_altera_emprestimo_emprestado();
	}
	public function acao_altera_totens(){
		$this->padrao_model->altera_totens();
	}
	public function deslogar(){
		$this->session->sess_destroy();
		redirect('padrao');
	}
}

 ?>
