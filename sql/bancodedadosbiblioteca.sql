-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.8-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para db_sistema_biblioteca
CREATE DATABASE IF NOT EXISTS `db_sistema_biblioteca` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_sistema_biblioteca`;

-- Copiando estrutura para tabela db_sistema_biblioteca.devolucao
CREATE TABLE IF NOT EXISTS `devolucao` (
  `idDevolucao` int(11) NOT NULL AUTO_INCREMENT,
  `idTotem_devolucao` int(11) DEFAULT NULL,
  `idLivro_devolucao` int(11) DEFAULT NULL,
  `horario_devolucao` time DEFAULT NULL,
  `data_devolucao` date DEFAULT NULL,
  PRIMARY KEY (`idDevolucao`),
  KEY `idTotem_devolucao` (`idTotem_devolucao`),
  KEY `idLivro_devolucao` (`idLivro_devolucao`),
  CONSTRAINT `idLivro_devolucao` FOREIGN KEY (`idLivro_devolucao`) REFERENCES `livro` (`idLivro`),
  CONSTRAINT `idTotem_devolucao` FOREIGN KEY (`idTotem_devolucao`) REFERENCES `totem` (`idTotem`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_sistema_biblioteca.devolucao: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `devolucao` DISABLE KEYS */;
INSERT IGNORE INTO `devolucao` (`idDevolucao`, `idTotem_devolucao`, `idLivro_devolucao`, `horario_devolucao`, `data_devolucao`) VALUES
	(1, 1, 1, '18:59:35', '2019-10-24'),
	(2, 1, 1, '20:14:48', '2019-10-02'),
	(3, 2, 1, '01:03:20', '2019-10-25');
/*!40000 ALTER TABLE `devolucao` ENABLE KEYS */;

-- Copiando estrutura para tabela db_sistema_biblioteca.emprestimo
CREATE TABLE IF NOT EXISTS `emprestimo` (
  `idEmprestimo` int(11) NOT NULL AUTO_INCREMENT,
  `idLivro_Emprestimo` int(11) DEFAULT NULL,
  `data_emprestimo` date DEFAULT NULL,
  `horario_emprestimo` time DEFAULT NULL,
  PRIMARY KEY (`idEmprestimo`),
  KEY `idLivro_Emprestimo` (`idLivro_Emprestimo`),
  CONSTRAINT `idLivro_Emprestimo` FOREIGN KEY (`idLivro_Emprestimo`) REFERENCES `livro` (`idLivro`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_sistema_biblioteca.emprestimo: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `emprestimo` DISABLE KEYS */;
INSERT IGNORE INTO `emprestimo` (`idEmprestimo`, `idLivro_Emprestimo`, `data_emprestimo`, `horario_emprestimo`) VALUES
	(2, 2, '2019-09-14', '17:21:55'),
	(5, 1, '2019-12-05', '17:31:16');
/*!40000 ALTER TABLE `emprestimo` ENABLE KEYS */;

-- Copiando estrutura para tabela db_sistema_biblioteca.historico_mes
CREATE TABLE IF NOT EXISTS `historico_mes` (
  `id_hist_mes` int(11) NOT NULL AUTO_INCREMENT,
  `qtd_livros_mes` int(11) NOT NULL,
  `emprestimos_mensais_mes` int(11) NOT NULL,
  `livros_alugados_mes` int(11) NOT NULL,
  `numero_mes` text DEFAULT NULL,
  `ano_mes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_hist_mes`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_sistema_biblioteca.historico_mes: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `historico_mes` DISABLE KEYS */;
INSERT IGNORE INTO `historico_mes` (`id_hist_mes`, `qtd_livros_mes`, `emprestimos_mensais_mes`, `livros_alugados_mes`, `numero_mes`, `ano_mes`) VALUES
	(1, 3, 5, 3, '11', 2019),
	(5, 3, 2, 2, '12', 2019),
	(6, 1, 1, 2, '10', 2019),
	(7, 2, 3, 3, '02', 2020);
/*!40000 ALTER TABLE `historico_mes` ENABLE KEYS */;

-- Copiando estrutura para tabela db_sistema_biblioteca.livro
CREATE TABLE IF NOT EXISTS `livro` (
  `idLivro` int(11) NOT NULL AUTO_INCREMENT,
  `nome_livro` varchar(70) NOT NULL,
  `autor_livro` varchar(30) NOT NULL,
  `id_rfid_livro` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`idLivro`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_sistema_biblioteca.livro: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `livro` DISABLE KEYS */;
INSERT IGNORE INTO `livro` (`idLivro`, `nome_livro`, `autor_livro`, `id_rfid_livro`) VALUES
	(1, 'Da Vinci', 'Dan Brown', '123456'),
	(2, 'Teste 2', 'Junior Menel', '213121'),
	(18, 'dfdf', 'df', '34342'),
	(19, 'g', 'g', '111'),
	(20, '3', '3', '333');
/*!40000 ALTER TABLE `livro` ENABLE KEYS */;

-- Copiando estrutura para tabela db_sistema_biblioteca.livros_a_cadastrar
CREATE TABLE IF NOT EXISTS `livros_a_cadastrar` (
  `idLivrosACadastrar` int(11) NOT NULL AUTO_INCREMENT,
  `id_rfid_livro` varchar(200) NOT NULL,
  PRIMARY KEY (`idLivrosACadastrar`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_sistema_biblioteca.livros_a_cadastrar: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `livros_a_cadastrar` DISABLE KEYS */;
/*!40000 ALTER TABLE `livros_a_cadastrar` ENABLE KEYS */;

-- Copiando estrutura para tabela db_sistema_biblioteca.livros_pendentes
CREATE TABLE IF NOT EXISTS `livros_pendentes` (
  `idLivrosPendentes` int(11) NOT NULL AUTO_INCREMENT,
  `idLivro_livros_pendentes` int(11) NOT NULL,
  PRIMARY KEY (`idLivrosPendentes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_sistema_biblioteca.livros_pendentes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `livros_pendentes` DISABLE KEYS */;
/*!40000 ALTER TABLE `livros_pendentes` ENABLE KEYS */;

-- Copiando estrutura para tabela db_sistema_biblioteca.totem
CREATE TABLE IF NOT EXISTS `totem` (
  `idTotem` int(11) NOT NULL AUTO_INCREMENT,
  `nome_totem` varchar(40) NOT NULL,
  `numero_serie_totem` varchar(20) NOT NULL,
  `lotacao_totem` int(11) NOT NULL,
  `maximo_totem` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`idTotem`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_sistema_biblioteca.totem: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `totem` DISABLE KEYS */;
INSERT IGNORE INTO `totem` (`idTotem`, `nome_totem`, `numero_serie_totem`, `lotacao_totem`, `maximo_totem`) VALUES
	(1, 'Predio H', '123456789', 4, 20),
	(2, 'Biologia', '123456790', 7, 15);
/*!40000 ALTER TABLE `totem` ENABLE KEYS */;

-- Copiando estrutura para tabela db_sistema_biblioteca.user
CREATE TABLE IF NOT EXISTS `user` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `login_user` varchar(30) NOT NULL,
  `senha_user` varchar(150) NOT NULL,
  `nome_user` varchar(50) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_sistema_biblioteca.user: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT IGNORE INTO `user` (`idUsuario`, `login_user`, `senha_user`, `nome_user`) VALUES
	(1, 'joaop', '123', 'Joao');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
